<?php

  $array_product = array(
    '0'=> array(
      'productInfo' => array(
        'id'=>'SPM',
        'name'=>'Sijil Pelajaran Malaysia',
        'currency' => 'USD',
        'price'=>'269.99'
      )
    ),
    '1'=> array(
      'productInfo' => array(
        'id'=>'PT3',
        'name'=>'Penilaian Menengah Rendah',
        'currency' => 'USD',
        'price'=>'322.99'
      )
    ),
    '2'=> array(
      'productInfo' => array(
        'id'=>'LPUPSR',
        'name'=>'Lower Primary & Upper Primary',
        'currency' => 'USD',
        'price'=>'394.99'
      )
    )
  );

  echo json_encode($array_product);

?>
