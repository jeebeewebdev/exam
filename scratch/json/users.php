<?php

  $array_user = array(
    '0'=> array(
      'account' => array(
        'username'=>'user1',
        'password'=>'password',
        'userType'=>'college'
      ),
      'profile' => array(
        'fname'=>'Isabella',
        'lname'=>'Sander',
        'gender'=>'Female'
      )
    ),
    '1'=> array(
      'account' => array(
        'username'=>'user2',
        'password'=>'password',
        'userType'=>'students'
      ),
      'profile' => array(
        'fname'=>'Jacob',
        'lname'=>'Sander',
        'gender'=>'Male'
      )
    ),
    '2'=> array(
      'account' => array(
        'username'=>'user3',
        'password'=>'password',
        'userType'=>'schools'
      ),
      'profile' => array(
        'fname'=>'Jacob',
        'lname'=>'Sander',
        'gender'=>'Male'
      )
    ),
    '3'=> array(
      'account' => array(
        'username'=>'user4',
        'password'=>'password',
        'userType'=>'default'
      ),
      'profile' => array(
        'fname'=>'Jacob',
        'lname'=>'Sander',
        'gender'=>'Male'
      )
    )
  );

  echo json_encode($array_user);

?>
