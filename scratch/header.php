<?php require_once('function.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Exercise</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/vendor/fontawesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />

  </head>
